import Vue from 'vue'
import VueRouter from 'vue-router'

//Components
import UserRoleManagement from '../components/UserRoleManagement.vue'
import AddRole from '../components/AddRole.vue'
import EditRole from '../components/EditRole.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/', component: UserRoleManagement },
    { path: '/role/add', component: AddRole },
    { path: '/role/edit/:id', component: EditRole },
    { path: '*', component: UserRoleManagement }
];

const router = new VueRouter({
    routes,
    mode: 'history',
})

export default router