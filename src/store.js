import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
    key: 'keepersolutions',
    storage: window.sessionStorage
});

const store = new Vuex.Store({
    plugins: [vuexLocal.plugin],
    state: {
        userRolesArray: [{
            id: 1,
            name: "Super Admin",
            type: "admin",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            editable: false,
            active: true,
            created_on: "2019-01-18T18:25:43.511Z",
        },
        {
            id: 2,
            name: "Company Admin",
            type: "admin",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            editable: false,
            active: true,
            created_on: "2019-01-18T18:25:43.511Z",
        },
        {
            id: 3,
            name: "Property Admin",
            type: "admin",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            editable: false,
            active: false,
            created_on: "2019-01-18T18:25:43.511Z",
        },
        {
            id: 4,
            name: "Job Recruiter",
            type: "job_admin",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            editable: true,
            active: true,
            created_on: "2019-01-18T18:25:43.511Z",
        },
        {
            id: 5,
            name: "Content Administrator",
            type: "admin",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            editable: true,
            active: true,
            created_on: "2019-01-18T18:25:43.511Z",
        },
        {
            id: 6,
            name: "Property Moderator",
            type: "admin",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            editable: false,
            active: true,
            created_on: "2019-01-18T18:25:43.511Z"
        },
        ],
    },
    mutations: {
        saveRole(state, role) {
            state.userRolesArray.push(role);
        },
        editRole(state, role) {
            Vue.set(state.userRolesArray, role.id, { ...state.userRolesArray[role.id], ...role });
        }
    },
    actions: {
        filterArray({ state }, searchText) {
            let filterArr = [];

            if (searchText.length > 0) {
                filterArr = state.userRolesArray.filter(function (userRole) {
                    return userRole.name
                        .toLowerCase()
                        .includes(self.searchText.toLowerCase());
                });
            } else {
                filterArr = state.userRolesArray;
            }

            return filterArr;
        }
    },
    getters: {
        getuserRolesArray: state => state.userRolesArray
    }
});

export default store;