import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import store from './store';
import Vuelidate from 'vuelidate';
import VueSimpleAlert from 'vue-simple-alert';

Vue.config.productionTip = false

Vue.use(Vuelidate);
Vue.use(require("vue-moment"));
Vue.use(VueSimpleAlert);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
